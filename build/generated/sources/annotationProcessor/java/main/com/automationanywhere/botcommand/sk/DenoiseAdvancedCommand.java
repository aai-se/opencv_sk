package com.automationanywhere.botcommand.sk;

import com.automationanywhere.bot.service.GlobalSessionContext;
import com.automationanywhere.botcommand.BotCommand;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import java.lang.ClassCastException;
import java.lang.Deprecated;
import java.lang.Number;
import java.lang.Object;
import java.lang.String;
import java.lang.Throwable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class DenoiseAdvancedCommand implements BotCommand {
  private static final Logger logger = LogManager.getLogger(DenoiseAdvancedCommand.class);

  private static final Messages MESSAGES_GENERIC = MessagesFactory.getMessages("com.automationanywhere.commandsdk.generic.messages");

  @Deprecated
  public Optional<Value> execute(Map<String, Value> parameters, Map<String, Object> sessionMap) {
    return execute(null, parameters, sessionMap);
  }

  public Optional<Value> execute(GlobalSessionContext globalSessionContext,
      Map<String, Value> parameters, Map<String, Object> sessionMap) {
    logger.traceEntry(() -> parameters != null ? parameters.entrySet().stream().filter(en -> !Arrays.asList( new String[] {}).contains(en.getKey()) && en.getValue() != null).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)).toString() : null, ()-> sessionMap != null ?sessionMap.toString() : null);
    DenoiseAdvanced command = new DenoiseAdvanced();
    HashMap<String, Object> convertedParameters = new HashMap<String, Object>();
    if(parameters.containsKey("imagefile") && parameters.get("imagefile") != null && parameters.get("imagefile").get() != null) {
      convertedParameters.put("imagefile", parameters.get("imagefile").get());
      if(convertedParameters.get("imagefile") !=null && !(convertedParameters.get("imagefile") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","imagefile", "String", parameters.get("imagefile").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("imagefile") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","imagefile"));
    }

    if(parameters.containsKey("savefile") && parameters.get("savefile") != null && parameters.get("savefile").get() != null) {
      convertedParameters.put("savefile", parameters.get("savefile").get());
      if(convertedParameters.get("savefile") !=null && !(convertedParameters.get("savefile") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","savefile", "String", parameters.get("savefile").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("savefile") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","savefile"));
    }

    if(parameters.containsKey("threshold") && parameters.get("threshold") != null && parameters.get("threshold").get() != null) {
      convertedParameters.put("threshold", parameters.get("threshold").get());
      if(convertedParameters.get("threshold") !=null && !(convertedParameters.get("threshold") instanceof Number)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","threshold", "Number", parameters.get("threshold").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("threshold") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","threshold"));
    }

    if(parameters.containsKey("kernel") && parameters.get("kernel") != null && parameters.get("kernel").get() != null) {
      convertedParameters.put("kernel", parameters.get("kernel").get());
      if(convertedParameters.get("kernel") !=null && !(convertedParameters.get("kernel") instanceof Number)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","kernel", "Number", parameters.get("kernel").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("kernel") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","kernel"));
    }

    if(parameters.containsKey("blur") && parameters.get("blur") != null && parameters.get("blur").get() != null) {
      convertedParameters.put("blur", parameters.get("blur").get());
      if(convertedParameters.get("blur") !=null && !(convertedParameters.get("blur") instanceof Number)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","blur", "Number", parameters.get("blur").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("blur") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","blur"));
    }

    try {
      command.action((String)convertedParameters.get("imagefile"),(String)convertedParameters.get("savefile"),(Number)convertedParameters.get("threshold"),(Number)convertedParameters.get("kernel"),(Number)convertedParameters.get("blur"));Optional<Value> result = Optional.empty();
      return logger.traceExit(result);
    }
    catch (ClassCastException e) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.IllegalParameters","action"));
    }
    catch (BotCommandException e) {
      logger.fatal(e.getMessage(),e);
      throw e;
    }
    catch (Throwable e) {
      logger.fatal(e.getMessage(),e);
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.NotBotCommandException",e.getMessage()),e);
    }
  }

  public Map<String, Value> executeAndReturnMany(GlobalSessionContext globalSessionContext,
      Map<String, Value> parameters, Map<String, Object> sessionMap) {
    return null;
  }
}
