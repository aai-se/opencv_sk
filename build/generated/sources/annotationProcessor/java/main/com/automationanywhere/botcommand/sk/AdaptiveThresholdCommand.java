package com.automationanywhere.botcommand.sk;

import com.automationanywhere.bot.service.GlobalSessionContext;
import com.automationanywhere.botcommand.BotCommand;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import java.lang.Boolean;
import java.lang.ClassCastException;
import java.lang.Deprecated;
import java.lang.Number;
import java.lang.Object;
import java.lang.String;
import java.lang.Throwable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class AdaptiveThresholdCommand implements BotCommand {
  private static final Logger logger = LogManager.getLogger(AdaptiveThresholdCommand.class);

  private static final Messages MESSAGES_GENERIC = MessagesFactory.getMessages("com.automationanywhere.commandsdk.generic.messages");

  @Deprecated
  public Optional<Value> execute(Map<String, Value> parameters, Map<String, Object> sessionMap) {
    return execute(null, parameters, sessionMap);
  }

  public Optional<Value> execute(GlobalSessionContext globalSessionContext,
      Map<String, Value> parameters, Map<String, Object> sessionMap) {
    logger.traceEntry(() -> parameters != null ? parameters.entrySet().stream().filter(en -> !Arrays.asList( new String[] {}).contains(en.getKey()) && en.getValue() != null).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)).toString() : null, ()-> sessionMap != null ?sessionMap.toString() : null);
    AdaptiveThreshold command = new AdaptiveThreshold();
    HashMap<String, Object> convertedParameters = new HashMap<String, Object>();
    if(parameters.containsKey("imagefile") && parameters.get("imagefile") != null && parameters.get("imagefile").get() != null) {
      convertedParameters.put("imagefile", parameters.get("imagefile").get());
      if(convertedParameters.get("imagefile") !=null && !(convertedParameters.get("imagefile") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","imagefile", "String", parameters.get("imagefile").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("imagefile") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","imagefile"));
    }

    if(parameters.containsKey("savefile") && parameters.get("savefile") != null && parameters.get("savefile").get() != null) {
      convertedParameters.put("savefile", parameters.get("savefile").get());
      if(convertedParameters.get("savefile") !=null && !(convertedParameters.get("savefile") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","savefile", "String", parameters.get("savefile").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("savefile") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","savefile"));
    }

    if(parameters.containsKey("blockSize") && parameters.get("blockSize") != null && parameters.get("blockSize").get() != null) {
      convertedParameters.put("blockSize", parameters.get("blockSize").get());
      if(convertedParameters.get("blockSize") !=null && !(convertedParameters.get("blockSize") instanceof Number)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","blockSize", "Number", parameters.get("blockSize").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("blockSize") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","blockSize"));
    }

    if(parameters.containsKey("C") && parameters.get("C") != null && parameters.get("C").get() != null) {
      convertedParameters.put("C", parameters.get("C").get());
      if(convertedParameters.get("C") !=null && !(convertedParameters.get("C") instanceof Number)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","C", "Number", parameters.get("C").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("C") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","C"));
    }

    if(parameters.containsKey("ApplyBlur") && parameters.get("ApplyBlur") != null && parameters.get("ApplyBlur").get() != null) {
      convertedParameters.put("ApplyBlur", parameters.get("ApplyBlur").get());
      if(convertedParameters.get("ApplyBlur") !=null && !(convertedParameters.get("ApplyBlur") instanceof Boolean)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","ApplyBlur", "Boolean", parameters.get("ApplyBlur").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("ApplyBlur") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","ApplyBlur"));
    }

    if(parameters.containsKey("InvertImage") && parameters.get("InvertImage") != null && parameters.get("InvertImage").get() != null) {
      convertedParameters.put("InvertImage", parameters.get("InvertImage").get());
      if(convertedParameters.get("InvertImage") !=null && !(convertedParameters.get("InvertImage") instanceof Boolean)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","InvertImage", "Boolean", parameters.get("InvertImage").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("InvertImage") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","InvertImage"));
    }

    try {
      command.action((String)convertedParameters.get("imagefile"),(String)convertedParameters.get("savefile"),(Number)convertedParameters.get("blockSize"),(Number)convertedParameters.get("C"),(Boolean)convertedParameters.get("ApplyBlur"),(Boolean)convertedParameters.get("InvertImage"));Optional<Value> result = Optional.empty();
      return logger.traceExit(result);
    }
    catch (ClassCastException e) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.IllegalParameters","action"));
    }
    catch (BotCommandException e) {
      logger.fatal(e.getMessage(),e);
      throw e;
    }
    catch (Throwable e) {
      logger.fatal(e.getMessage(),e);
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.NotBotCommandException",e.getMessage()),e);
    }
  }

  public Map<String, Value> executeAndReturnMany(GlobalSessionContext globalSessionContext,
      Map<String, Value> parameters, Map<String, Object> sessionMap) {
    return null;
  }
}
