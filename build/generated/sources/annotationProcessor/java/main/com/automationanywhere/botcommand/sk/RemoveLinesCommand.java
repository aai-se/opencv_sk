package com.automationanywhere.botcommand.sk;

import com.automationanywhere.bot.service.GlobalSessionContext;
import com.automationanywhere.botcommand.BotCommand;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import java.lang.ClassCastException;
import java.lang.Deprecated;
import java.lang.Number;
import java.lang.Object;
import java.lang.String;
import java.lang.Throwable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class RemoveLinesCommand implements BotCommand {
  private static final Logger logger = LogManager.getLogger(RemoveLinesCommand.class);

  private static final Messages MESSAGES_GENERIC = MessagesFactory.getMessages("com.automationanywhere.commandsdk.generic.messages");

  @Deprecated
  public Optional<Value> execute(Map<String, Value> parameters, Map<String, Object> sessionMap) {
    return execute(null, parameters, sessionMap);
  }

  public Optional<Value> execute(GlobalSessionContext globalSessionContext,
      Map<String, Value> parameters, Map<String, Object> sessionMap) {
    logger.traceEntry(() -> parameters != null ? parameters.entrySet().stream().filter(en -> !Arrays.asList( new String[] {}).contains(en.getKey()) && en.getValue() != null).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)).toString() : null, ()-> sessionMap != null ?sessionMap.toString() : null);
    RemoveLines command = new RemoveLines();
    HashMap<String, Object> convertedParameters = new HashMap<String, Object>();
    if(parameters.containsKey("imagefile") && parameters.get("imagefile") != null && parameters.get("imagefile").get() != null) {
      convertedParameters.put("imagefile", parameters.get("imagefile").get());
      if(convertedParameters.get("imagefile") !=null && !(convertedParameters.get("imagefile") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","imagefile", "String", parameters.get("imagefile").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("imagefile") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","imagefile"));
    }

    if(parameters.containsKey("savefile") && parameters.get("savefile") != null && parameters.get("savefile").get() != null) {
      convertedParameters.put("savefile", parameters.get("savefile").get());
      if(convertedParameters.get("savefile") !=null && !(convertedParameters.get("savefile") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","savefile", "String", parameters.get("savefile").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("savefile") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","savefile"));
    }

    if(parameters.containsKey("horzlength") && parameters.get("horzlength") != null && parameters.get("horzlength").get() != null) {
      convertedParameters.put("horzlength", parameters.get("horzlength").get());
      if(convertedParameters.get("horzlength") !=null && !(convertedParameters.get("horzlength") instanceof Number)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","horzlength", "Number", parameters.get("horzlength").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("horzlength") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","horzlength"));
    }

    if(parameters.containsKey("vertlength") && parameters.get("vertlength") != null && parameters.get("vertlength").get() != null) {
      convertedParameters.put("vertlength", parameters.get("vertlength").get());
      if(convertedParameters.get("vertlength") !=null && !(convertedParameters.get("vertlength") instanceof Number)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","vertlength", "Number", parameters.get("vertlength").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("vertlength") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","vertlength"));
    }

    try {
      command.action((String)convertedParameters.get("imagefile"),(String)convertedParameters.get("savefile"),(Number)convertedParameters.get("horzlength"),(Number)convertedParameters.get("vertlength"));Optional<Value> result = Optional.empty();
      return logger.traceExit(result);
    }
    catch (ClassCastException e) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.IllegalParameters","action"));
    }
    catch (BotCommandException e) {
      logger.fatal(e.getMessage(),e);
      throw e;
    }
    catch (Throwable e) {
      logger.fatal(e.getMessage(),e);
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.NotBotCommandException",e.getMessage()),e);
    }
  }

  public Map<String, Value> executeAndReturnMany(GlobalSessionContext globalSessionContext,
      Map<String, Value> parameters, Map<String, Object> sessionMap) {
    return null;
  }
}
