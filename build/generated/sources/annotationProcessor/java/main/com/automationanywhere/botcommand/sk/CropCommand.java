package com.automationanywhere.botcommand.sk;

import com.automationanywhere.bot.service.GlobalSessionContext;
import com.automationanywhere.botcommand.BotCommand;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import java.lang.ClassCastException;
import java.lang.Deprecated;
import java.lang.Number;
import java.lang.Object;
import java.lang.String;
import java.lang.Throwable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class CropCommand implements BotCommand {
  private static final Logger logger = LogManager.getLogger(CropCommand.class);

  private static final Messages MESSAGES_GENERIC = MessagesFactory.getMessages("com.automationanywhere.commandsdk.generic.messages");

  @Deprecated
  public Optional<Value> execute(Map<String, Value> parameters, Map<String, Object> sessionMap) {
    return execute(null, parameters, sessionMap);
  }

  public Optional<Value> execute(GlobalSessionContext globalSessionContext,
      Map<String, Value> parameters, Map<String, Object> sessionMap) {
    logger.traceEntry(() -> parameters != null ? parameters.entrySet().stream().filter(en -> !Arrays.asList( new String[] {}).contains(en.getKey()) && en.getValue() != null).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)).toString() : null, ()-> sessionMap != null ?sessionMap.toString() : null);
    Crop command = new Crop();
    HashMap<String, Object> convertedParameters = new HashMap<String, Object>();
    if(parameters.containsKey("imagefile") && parameters.get("imagefile") != null && parameters.get("imagefile").get() != null) {
      convertedParameters.put("imagefile", parameters.get("imagefile").get());
      if(convertedParameters.get("imagefile") !=null && !(convertedParameters.get("imagefile") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","imagefile", "String", parameters.get("imagefile").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("imagefile") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","imagefile"));
    }

    if(parameters.containsKey("savefile1") && parameters.get("savefile1") != null && parameters.get("savefile1").get() != null) {
      convertedParameters.put("savefile1", parameters.get("savefile1").get());
      if(convertedParameters.get("savefile1") !=null && !(convertedParameters.get("savefile1") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","savefile1", "String", parameters.get("savefile1").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("savefile1") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","savefile1"));
    }

    if(parameters.containsKey("savefile2") && parameters.get("savefile2") != null && parameters.get("savefile2").get() != null) {
      convertedParameters.put("savefile2", parameters.get("savefile2").get());
      if(convertedParameters.get("savefile2") !=null && !(convertedParameters.get("savefile2") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","savefile2", "String", parameters.get("savefile2").get().getClass().getSimpleName()));
      }
    }

    if(parameters.containsKey("savefile3") && parameters.get("savefile3") != null && parameters.get("savefile3").get() != null) {
      convertedParameters.put("savefile3", parameters.get("savefile3").get());
      if(convertedParameters.get("savefile3") !=null && !(convertedParameters.get("savefile3") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","savefile3", "String", parameters.get("savefile3").get().getClass().getSimpleName()));
      }
    }

    if(parameters.containsKey("threshold") && parameters.get("threshold") != null && parameters.get("threshold").get() != null) {
      convertedParameters.put("threshold", parameters.get("threshold").get());
      if(convertedParameters.get("threshold") !=null && !(convertedParameters.get("threshold") instanceof Number)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","threshold", "Number", parameters.get("threshold").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("threshold") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","threshold"));
    }

    try {
      command.action((String)convertedParameters.get("imagefile"),(String)convertedParameters.get("savefile1"),(String)convertedParameters.get("savefile2"),(String)convertedParameters.get("savefile3"),(Number)convertedParameters.get("threshold"));Optional<Value> result = Optional.empty();
      return logger.traceExit(result);
    }
    catch (ClassCastException e) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.IllegalParameters","action"));
    }
    catch (BotCommandException e) {
      logger.fatal(e.getMessage(),e);
      throw e;
    }
    catch (Throwable e) {
      logger.fatal(e.getMessage(),e);
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.NotBotCommandException",e.getMessage()),e);
    }
  }

  public Map<String, Value> executeAndReturnMany(GlobalSessionContext globalSessionContext,
      Map<String, Value> parameters, Map<String, Object> sessionMap) {
    return null;
  }
}
