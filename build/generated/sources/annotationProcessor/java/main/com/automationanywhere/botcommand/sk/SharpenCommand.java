package com.automationanywhere.botcommand.sk;

import com.automationanywhere.bot.service.GlobalSessionContext;
import com.automationanywhere.botcommand.BotCommand;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import java.lang.ClassCastException;
import java.lang.Deprecated;
import java.lang.Number;
import java.lang.Object;
import java.lang.String;
import java.lang.Throwable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class SharpenCommand implements BotCommand {
  private static final Logger logger = LogManager.getLogger(SharpenCommand.class);

  private static final Messages MESSAGES_GENERIC = MessagesFactory.getMessages("com.automationanywhere.commandsdk.generic.messages");

  @Deprecated
  public Optional<Value> execute(Map<String, Value> parameters, Map<String, Object> sessionMap) {
    return execute(null, parameters, sessionMap);
  }

  public Optional<Value> execute(GlobalSessionContext globalSessionContext,
      Map<String, Value> parameters, Map<String, Object> sessionMap) {
    logger.traceEntry(() -> parameters != null ? parameters.entrySet().stream().filter(en -> !Arrays.asList( new String[] {}).contains(en.getKey()) && en.getValue() != null).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)).toString() : null, ()-> sessionMap != null ?sessionMap.toString() : null);
    Sharpen command = new Sharpen();
    HashMap<String, Object> convertedParameters = new HashMap<String, Object>();
    if(parameters.containsKey("imagefile") && parameters.get("imagefile") != null && parameters.get("imagefile").get() != null) {
      convertedParameters.put("imagefile", parameters.get("imagefile").get());
      if(convertedParameters.get("imagefile") !=null && !(convertedParameters.get("imagefile") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","imagefile", "String", parameters.get("imagefile").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("imagefile") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","imagefile"));
    }

    if(parameters.containsKey("savefile") && parameters.get("savefile") != null && parameters.get("savefile").get() != null) {
      convertedParameters.put("savefile", parameters.get("savefile").get());
      if(convertedParameters.get("savefile") !=null && !(convertedParameters.get("savefile") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","savefile", "String", parameters.get("savefile").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("savefile") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","savefile"));
    }

    if(parameters.containsKey("sigma") && parameters.get("sigma") != null && parameters.get("sigma").get() != null) {
      convertedParameters.put("sigma", parameters.get("sigma").get());
      if(convertedParameters.get("sigma") !=null && !(convertedParameters.get("sigma") instanceof Number)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","sigma", "Number", parameters.get("sigma").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("sigma") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","sigma"));
    }

    if(parameters.containsKey("alpha") && parameters.get("alpha") != null && parameters.get("alpha").get() != null) {
      convertedParameters.put("alpha", parameters.get("alpha").get());
      if(convertedParameters.get("alpha") !=null && !(convertedParameters.get("alpha") instanceof Number)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","alpha", "Number", parameters.get("alpha").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("alpha") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","alpha"));
    }

    if(parameters.containsKey("beta") && parameters.get("beta") != null && parameters.get("beta").get() != null) {
      convertedParameters.put("beta", parameters.get("beta").get());
      if(convertedParameters.get("beta") !=null && !(convertedParameters.get("beta") instanceof Number)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","beta", "Number", parameters.get("beta").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("beta") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","beta"));
    }

    if(parameters.containsKey("gamma") && parameters.get("gamma") != null && parameters.get("gamma").get() != null) {
      convertedParameters.put("gamma", parameters.get("gamma").get());
      if(convertedParameters.get("gamma") !=null && !(convertedParameters.get("gamma") instanceof Number)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","gamma", "Number", parameters.get("gamma").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("gamma") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","gamma"));
    }

    try {
      command.action((String)convertedParameters.get("imagefile"),(String)convertedParameters.get("savefile"),(Number)convertedParameters.get("sigma"),(Number)convertedParameters.get("alpha"),(Number)convertedParameters.get("beta"),(Number)convertedParameters.get("gamma"));Optional<Value> result = Optional.empty();
      return logger.traceExit(result);
    }
    catch (ClassCastException e) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.IllegalParameters","action"));
    }
    catch (BotCommandException e) {
      logger.fatal(e.getMessage(),e);
      throw e;
    }
    catch (Throwable e) {
      logger.fatal(e.getMessage(),e);
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.NotBotCommandException",e.getMessage()),e);
    }
  }

  public Map<String, Value> executeAndReturnMany(GlobalSessionContext globalSessionContext,
      Map<String, Value> parameters, Map<String, Object> sessionMap) {
    return null;
  }
}
