/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 * 
 */
package com.automationanywhere.botcommand.sk;

import static com.automationanywhere.commandsdk.model.AttributeType.NUMBER;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;

import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.commandsdk.annotations.Execute;

/**
 * @author Stefan Karsten
 *
 */

@BotCommand
@CommandPkg(label = "Remove Lines", name = "removelinesimage",
        description = "Remove Lines from a Image",
        node_label = "Remove Lines from a Image",icon = "pkg.svg")
public class RemoveLines  {


		@Execute
         public void action (@Idx(index = "1", type = AttributeType.FILE)  @Pkg(label = "Orig. Image" , default_value_type =  DataType.FILE) @NotEmpty String imagefile,
        		                     @Idx(index = "2", type = AttributeType.FILE)  @Pkg(label = "Output Image" , default_value_type =  DataType.FILE) @NotEmpty String savefile,
        		        			@Idx(index = "3", type = AttributeType.NUMBER) @Pkg(label = "Horizontal Divider", default_value_type = DataType.NUMBER) @NotEmpty Number horzlength,
        		        			@Idx(index = "4", type = AttributeType.NUMBER) @Pkg(label = "Vertical Divider", default_value_type = DataType.NUMBER) @NotEmpty Number vertlength)
         {    
        	 
        	 
   		  			System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
;
            
                     //Load image file
                     Mat source=Imgcodecs.imread(imagefile);
                     
                     Imgproc.cvtColor(source,source,Imgproc.COLOR_BGR2GRAY);
                     
                     
                     org.opencv.core.Core.bitwise_not(source, source);
                     
     
                 
                     Mat th2 = new Mat();
 					 Imgproc.adaptiveThreshold(source,th2 ,255, Imgproc.ADAPTIVE_THRESH_MEAN_C,Imgproc.THRESH_BINARY,15,-2);
 					
 		            
                     
                     Mat horizontal = th2.clone();
                     Mat vertical = th2.clone();
                     int rows = horizontal.rows();
                     int cols = horizontal.cols();
                     int horizontalsize = Math.abs(cols / horzlength.intValue());
                     Mat horizontalStructure = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(horizontalsize,1));
                     Point anchor = new Point(-1, -1);
                     Imgproc.erode(horizontal, horizontal, horizontalStructure, anchor);
                     Imgproc.dilate(horizontal, horizontal, horizontalStructure, anchor);
                     
                     
                     int verticalsize = Math.abs(rows / vertlength.intValue());
                     Mat verticalStructure = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(1, verticalsize));
                     Imgproc.erode(vertical,vertical , verticalStructure, anchor);
                     Imgproc.dilate(vertical, vertical ,verticalStructure, anchor);
  
                     Mat horizontal_inv = new Mat();;
                     org.opencv.core.Core.bitwise_not(horizontal,horizontal_inv );
                     Mat masked_img = new Mat();
                     Core.bitwise_and(source, source, masked_img,horizontal_inv);
                     Mat masked_img_inv = new Mat();
                     Core.bitwise_not(masked_img,masked_img_inv );
 	               
                     source = masked_img_inv.clone();
                     org.opencv.core.Core.bitwise_not(source, source);
 	               
                     Mat vertical_inv = new Mat();
                     org.opencv.core.Core.bitwise_not(vertical,vertical_inv );
                     masked_img = new Mat();
                     Core.bitwise_and(source, source, masked_img,vertical_inv);
                     masked_img_inv = new Mat();
                     Core.bitwise_not(masked_img,masked_img_inv );
 	               
 	          
 	               Imgcodecs.imwrite(savefile,masked_img_inv);

         }


}
	
