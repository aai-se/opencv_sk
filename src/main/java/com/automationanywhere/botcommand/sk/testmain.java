package com.automationanywhere.botcommand.sk;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Vector;
import java.util.stream.Collectors;

import org.opencv.calib3d.Calib3d;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.DMatch;
import org.opencv.core.KeyPoint;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.MatOfDMatch;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.RotatedRect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.core.TermCriteria;
import org.opencv.features2d.BFMatcher;
import org.opencv.features2d.DescriptorMatcher;
import org.opencv.features2d.Features2d;
import org.opencv.features2d.ORB;
import org.opencv.highgui.HighGui;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.photo.Photo;
import org.opencv.video.Video;
import org.opencv.core.Core.MinMaxLocResult;

import org.opencv.photo.Photo.*;

public class testmain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		String imagefile = "C:\\Users\\Stefan Karsten\\Documents\\AlleDateien\\Kunden\\WHO\\OpenCV\\Pics\\ID__4_AdvancedDenoise.jpg";
		String imagereffile = "C:\\temp\\checkbox.jpg";
		
		String savefile1 = "C:\\temp\\save1.jpeg";	
		String savefile2 = "C:\\temp\\save2.jpeg";	
		String savefile3 = "C:\\temp\\save3.jpeg";	
		String savefile4 = "C:\\temp\\save4.jpeg";	
		String verticalf = "C:\\temp\\savev.jpeg";
		String horizontalf = "C:\\temp\\saveh.jpeg";
		
		
		  	System.loadLibrary(Core.NATIVE_LIBRARY_NAME);

		  	
            Mat image = Imgcodecs.imread(imagefile);
            
            Mat imagebw = Mat.zeros(image.size(), image.type());
            
            HighGui.imshow("New ImageDav", image);
            ;
                    Mat source=null;
                    Mat template=null;
                    //Load image file
                    source=Imgcodecs.imread(imagefile);
                    
                    Imgproc.cvtColor(source,source,Imgproc.COLOR_BGR2GRAY);
                    
                    
                    org.opencv.core.Core.bitwise_not(source, source);
                    
    
                
                    Mat th2 = new Mat();
					Imgproc.adaptiveThreshold(source,th2 ,255, Imgproc.ADAPTIVE_THRESH_MEAN_C,Imgproc.THRESH_BINARY,15,40);
					
		            
                    
                    Mat horizontal = th2.clone();
                    Mat vertical = th2.clone();
                    int rows = horizontal.rows();
                    int cols = horizontal.cols();
                    int horizontalsize = Math.abs(cols / 100);
                    Mat horizontalStructure = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(horizontalsize,1));
                    Point anchor = new Point(-1, -1);
                    Imgproc.erode(horizontal, horizontal, horizontalStructure, anchor);
                    Imgproc.dilate(horizontal, horizontal, horizontalStructure, anchor);
                    
                    
                    int verticalsize = Math.abs(rows / 100);
                    Mat verticalStructure = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(1, verticalsize));
                    Imgproc.erode(vertical,vertical , verticalStructure, anchor);
                    Imgproc.dilate(vertical, vertical ,verticalStructure, anchor);
                    //Imgproc.bitwise_not(vertical,vertical);
                    //org.opencv.core.Core.bitwise_not(vertical,vertical);
                    
                    Imgcodecs.imwrite(verticalf, vertical);
                    Imgcodecs.imwrite(horizontalf, horizontal);
                    
                    
    /*               Mat edges = new Mat();
                   Imgproc.adaptiveThreshold(vertical,edges ,255, Imgproc.ADAPTIVE_THRESH_MEAN_C,Imgproc.THRESH_BINARY,3,-2);

    
                   
                   
                   Mat kernel = Mat.ones(new Size(2, 2), CvType.CV_8UC1); 
                   Mat dilated = new Mat();
                   Imgproc.dilate(edges,dilated , kernel);
                   
	           
	               
	               
	               Mat smooth = new Mat();;
	               vertical.copyTo(smooth );
				
	               Imgproc.blur(smooth,smooth , new Size(2,2));
	               
	              */ 
	               Mat horizontal_inv = new Mat();;
	               org.opencv.core.Core.bitwise_not(horizontal,horizontal_inv );
	               Mat masked_img = new Mat();
	               Core.bitwise_and(source, source, masked_img,horizontal_inv);
	               Mat clone = masked_img.clone();
	               Mat masked_img_inv = new Mat();
	               Core.bitwise_not(masked_img,masked_img_inv );
	               
	               
	               Imgcodecs.imwrite(savefile1,masked_img);
	               Imgcodecs.imwrite(savefile2,horizontal_inv);
	               Imgcodecs.imwrite(savefile3,masked_img_inv);
	               
	               source = masked_img_inv.clone();
	               org.opencv.core.Core.bitwise_not(source, source);
	               
	               Mat vertical_inv = new Mat();
	               org.opencv.core.Core.bitwise_not(vertical,vertical_inv );
	               masked_img = new Mat();
	               Core.bitwise_and(source, source, masked_img,vertical_inv);
	               masked_img_inv = new Mat();
	               Core.bitwise_not(masked_img,masked_img_inv );
	               
	          
	               Imgcodecs.imwrite(savefile4,masked_img_inv);


                    System.out.println("Completed.");
 
			System.exit(0);
	      
	}
	
    private static byte saturate(double val) {
        int iVal = (int) Math.round(val);
        iVal = iVal > 255 ? 255 : (iVal < 0 ? 0 : iVal);
        return (byte) iVal;
    }
	
	
     
     

}
